import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import {Button} from "reactstrap";
import {connect} from "react-redux";

export class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <p>
            Edit <code>src/App.js</code> and save to reload.
          </p>
          <a
            className="App-link"
            href="https://reactjs.org"
            target="_blank"
            rel="noopener noreferrer"
          >
            Learn React
          </a>
          <Button>Hello {this.props.team}</Button>
        </header>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
    return {
        team: state.game.team1
    }
};

export default connect(
    mapStateToProps,
    null
)(App);
