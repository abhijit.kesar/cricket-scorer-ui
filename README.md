> This is intended to be used as a base for cricket scorer training app. Take a fork of this repo to continue any further development.

# Instructions


## Development

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).
### `yarn start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

### `yarn test`

Launches the test runner in the interactive watch mode.<br>
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.


## Styling

[Reactstrap](http://reactstrap.github.io/) is included to get out of the box basic [components](http://reactstrap.github.io/components/), [styling utilities](https://getbootstrap.com/docs/4.1/utilities/), and [layout](http://reactstrap.github.io/components/layout/). 


## CI/CD

[Gitlab CI/CD](https://docs.gitlab.com/ee/ci/) is used for a CI pipeline. Pipeline is configured using `.gitlab-ci.yml`.

## Deploying

[Gitlab Pages](https://docs.gitlab.com/ee/user/project/pages/) is used to host your app as a static website.

_Note: you need to edit package.json to update "homepage" to point to the pages url of your application._
